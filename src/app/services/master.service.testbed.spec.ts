import { MasterService } from './master.service'
import { ValueService } from './value.service'
import { TestBed } from '@angular/core/testing'
import { of } from 'rxjs'

describe('MasterService with TestBed', () => {
    let masterService: MasterService
    let valueServiceSpy: jasmine.SpyObj<ValueService>
    const stubValue = 'stub value'

    beforeEach(() => {
        const spy = jasmine.createSpyObj('ValueService', ['getValue', 'getObservableValue', 'getPromiseValue'])

        TestBed.configureTestingModule({
            providers: [
                MasterService,
                { provide: ValueService, useValue: spy }
            ]
        })
        masterService = TestBed.get(MasterService)
        valueServiceSpy = TestBed.get(ValueService)
    })

    it('getValue should return stubbed value from a spy', ()=> {
        valueServiceSpy.getValue.and.returnValue(stubValue)

        expect(masterService.getValue()).toBe(stubValue)

        expect(valueServiceSpy.getValue.calls.count()).toBe(1)
        expect(valueServiceSpy.getValue.calls.any()).toBeTruthy()
        expect(valueServiceSpy.getValue.calls.mostRecent().args).toEqual([])
        expect(valueServiceSpy.getValue.calls.mostRecent().returnValue).toBe(stubValue)
        expect(valueServiceSpy.getObservableValue.calls.count()).toBe(0)
    })

    it('getObservableValue should return stubbed value from a spy', (done:DoneFn)=> {
        valueServiceSpy.getObservableValue.and.returnValue(of(stubValue))

        masterService.getObservableValue().subscribe((res) => {
            expect(res).toBe(stubValue)
            done()

            expect(valueServiceSpy.getObservableValue.calls.count()).toBe(1)
            expect(valueServiceSpy.getObservableValue.calls.any()).toBeTruthy()
        })
    })

    it('getPromiseValue should return stubbed value from a spy', (done:DoneFn)=> {
        valueServiceSpy.getPromiseValue.and.returnValue(new Promise((resolve, reject) => resolve(stubValue)))

        masterService.getPromiseValue().then((res) => {
            expect(res).toBe(stubValue)
            done()
        })
    })
})