import { ValueService } from './value.service'
import { TestBed } from '@angular/core/testing'



describe('testing ValueService with TestBed', () => {

    let service:ValueService

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ValueService]
        })
        service = TestBed.get(ValueService)
    })

    it('getValue', ()=> {
        expect(service.getValue()).toBe('real value')
    })

    it('getObservableValue', (done:DoneFn) => {
        service.getObservableValue().subscribe((res)=> {
            expect(res).toBe('observable value')
            done()
        })
    })

    it('getPromiseValue', (done:DoneFn) => {
        service.getPromiseValue().then((res) => {
            expect(res).toBe('promise value')
            done()
        })
    })

})