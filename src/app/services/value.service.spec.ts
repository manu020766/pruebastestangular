import { ValueService } from './value.service'

describe('ValueService', () => {

  let service: ValueService
  beforeEach(() => {
    service = new ValueService()
  })

  it('return value string', () => {
    expect(service.getValue()).toBe('real value')
  })

  it('return value observable', (done: DoneFn) => {
    service.getObservableValue().subscribe((res) => {
      expect(res).toBe('observable value')
      done()
    })
  })

  it('return a promise', (done: DoneFn) => {
    service.getPromiseValue().then((res) => {
      expect(res).toBe('promise value')
      done()
    })
  })


})