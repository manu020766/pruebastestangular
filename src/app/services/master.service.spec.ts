import { MasterService } from './master.service'
import { ValueService } from './value.service'
import { of } from 'rxjs'

describe('MasterService', () => {
  let masterService: MasterService
  let fakeService: ValueService = {
    getValue: () => 'real value',
    getObservableValue: () => of('observable value'),
    getPromiseValue: () => new Promise((resolve, reject) => resolve('promise value'))
  }

  beforeEach(() => {
    masterService = new MasterService(fakeService)
  })

  it('Devuelve un string a traves de MasterService desde ValueService', () => {
    expect(masterService.getValue()).toBe('real value')
  })

  it('Devuelve un string a traves de MasterService desde un fake service', () => {
    expect(masterService.getValue()).toBe('real value')
  })

  it('Devuelve un observable a traves de MasterService desde un fake service', (done: DoneFn) => {
    masterService.getObservableValue().subscribe((res) => {
      expect(res).toBe('observable value')
      done()
    })
  })

  it('Devuelve una promesa a traves de MasterService desde un fake service parcial', (done: DoneFn) => {
    const fakePromise = {
      getPromiseValue: () => new Promise((resolve, reject) => resolve('promise value'))
    }

    const componente = new MasterService(fakePromise as ValueService)

    componente.getPromiseValue().then((res) => {
      expect(res).toBe('promise value')
      done()
    })
  })

  it('getValue from a spy', () => {
    const spyValueService = jasmine.createSpyObj('ValueService', ['getValue'])
    const stubValue = 'stub value'

    spyValueService.getValue.and.returnValue(stubValue)

    const componente = new MasterService(spyValueService)

    expect(componente.getValue()).toBe(stubValue)
  })

  it('getObservableValue and getPromiseValue from a spy', (done:DoneFn) => {
    const spyValue = jasmine.createSpyObj('ValueService', ['getObservableValue', 'getPromiseValue'])
    const stubValue = 'stub value'
    spyValue.getObservableValue.and.returnValue(of(stubValue))
    spyValue.getPromiseValue.and.returnValue(new Promise((resolve, reject) => resolve(stubValue)))
    const componente = new MasterService(spyValue)

    componente.getObservableValue().subscribe(res => {
      expect(res).toBe(stubValue)
      done()
    })

    componente.getPromiseValue().then((res) => {
      expect(res).toBe(stubValue)
      done()
    })
  })


})